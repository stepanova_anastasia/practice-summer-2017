#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <set>
#include <queue>
#include <stack>
#include <bitset>
#include <time.h>
#include <string>
#include <vector>
#include <math.h>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <time.h>

using namespace std;

typedef double ld;
typedef long long li;
typedef vector <int> vi;
typedef pair <ld, ld> pt;
typedef pair <int, int> pii;

#define ft first
#define sc second
#define mp make_pair
#define sqr(a) ((a) * (a))
#define sz(a) (int)(a.size())
#define all(a) a.begin(), a.end()
#define forn(i, n) for(int i = 0; i < int(n); i++)
#define fore(i,b,e) for(int i = int(b); i < int(e); i++)
const ld EPS = 1e-9;
const ld PI = acos(-1.0);
const int INF = int(1e9);
const li INF64 = li(1e18);
const int MOD = int(1e9) + 7;
const int N = 50 + 1;

vector<multiset<int>> g(N, multiset<int>());
vi path;

void fleury(int u)
{
	while (!g[u].empty())
	{
		int v = *g[u].begin();
		g[u].erase(g[u].begin());
		g[v].erase(find(all(g[v]), u));
		fleury(v);

	}

	path.push_back(u);

}


int main() {
#ifdef _DEBUG
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
#endif
	int n, m;
	cin >> n >> m;

	forn(i, m)
	{
		int v1, v2;
		scanf("%d%d", &v1, &v2);
		v1--, v2--;
		g[v1].insert(v2);
		g[v2].insert(v1);

	}

	int cnt = 0;
	int beg_ost = -1, beg = -1;
	forn(i, n)
	{
		if (int(sz(g[i])) % 2 == 1)
		{
			beg_ost = i;
			cnt++;
		}

		if (int(sz(g[i])) % 2 == 0 && int(sz(g[i])) != 0)
			beg = i;
	}

	if (beg_ost != -1)
		beg = beg_ost;

	fleury(beg);


	if (sz(path) < m + 1 || cnt > 2)
	{
		printf("-1");
		return 0;
	}

	cout << sz(path) << endl;
	reverse(all(path));

	forn(i, sz(path))
		printf("%d ", path[i] + 1);
	return 0;
}