#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <set>
#include <queue>
#include <stack>
#include <bitset>
#include <time.h>
#include <string>
#include <vector>
#include <math.h>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <time.h>

using namespace std;

typedef double ld;
typedef long long li;
typedef vector <int> vi;
typedef pair <ld, ld> pt;
typedef pair <int, int> pii;

#define ft first
#define sc second
#define mp make_pair
#define sqr(a) ((a) * (a))
#define sz(a) (int)(a.size())
#define all(a) a.begin(), a.end()
#define forn(i, n) for(int i = 0; i < int(n); i++)
#define fore(i,b,e) for(int i = int(b); i < int(e); i++)
const ld EPS = 1e-9;
const ld PI = acos(-1.0);
const int INF = int(1e9);
const li INF64 = li(1e18);
const int MOD = int(1e9) + 7;
const int N = 1e5 + 1;

int T = 0, cnt = 0;

void dfs(int u, vector<vector<int>> &g, map<pii, int> &a, vi &used)
{
	used[u] = cnt;
	forn(i, sz(g[u]))
	{
		int to = g[u][i];
		a[mp(u, to)] = 0;
		if (used[to] == -1)
			dfs(to, g, a, used);

	}

}



int main() {
#ifdef _DEBUG
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
#endif
	int t;
	cin >> t;
	forn(ti, t)
	{
		int n, m;
		scanf("%d%d", &n, &m);
		cnt = 0;
		vector<vector<int>> g1(n, vector<int>()), g2(n, vector<int>());
		map<pii, int> a;
		vi  used(n, -1);
		forn(i, m)
		{
			int v1, v2;
			scanf("%d%d", &v1, &v2);
			v1--, v2--;
			g1[v1].push_back(v2);
			g2[v2].push_back(v1);
			a[mp(v1, v2)] = -1;

		}


		int y = 0;
		pii x = mp(-1, -1);
		int v = -1;
		bool flag = true;
		forn(i, n)
		{

			if (abs(sz(g1[i]) - sz(g2[i])) == 1)
			{
				if (sz(g1[i]) < sz(g2[i]))
					x.ft = i;
				else
					x.sc = i;
				y++;
			}
			if (abs(sz(g1[i]) - sz(g2[i])) > 1)
				flag = false;
			if (sz(g1[i]) > 0)
				v = i;
		}
		if (x.ft != -1)
		{
			a[mp(x.ft, x.sc)] = -1;
			g1[x.ft].push_back(x.sc);
		}

		if (x.ft == -1)
			x.ft = v;

		if (flag && y <= 2 && x.ft != -1)
			dfs(x.ft, g1, a, used);
		for (auto it = a.begin(); it != a.end(); it++)
		{
			pair<pii, int> x = *it;
			if (x.sc == -1)
				flag = false;

		}

		if (y == 1 || y > 2 || !flag)
			printf("NO\n");
		else
			printf("YES\n");
	}
	return 0;
}