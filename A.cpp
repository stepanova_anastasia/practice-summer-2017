#define _CRT_SECURE_NO_WARNINGS

#include <map>
#include <set>
#include <queue>
#include <stack>
#include <bitset>
#include <time.h>
#include <string>
#include <vector>
#include <math.h>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <time.h>

using namespace std;

typedef double ld;
typedef long long li;
typedef vector <int> vi;
typedef pair <ld, ld> pt;
typedef pair <int, int> pii;

#define ft first
#define sc second
#define mp make_pair
#define sqr(a) ((a) * (a))
#define sz(a) (int)(a.size())
#define all(a) a.begin(), a.end()
#define forn(i, n) for(int i = 0; i < int(n); i++)
#define fore(i,b,e) for(int i = int(b); i < int(e); i++)
const ld EPS = 1e-9;
const ld PI = acos(-1.0);
const int INF = int(1e9);
const li INF64 = li(1e18);
const int MOD = int(1e9) + 7;
const int N = 1e5 * 2 + 1;

int cnt = 0;
void bfs(int u, vector<vi> &g, vi &d)
{
	queue<int> q;
	q.push(u);


	while (!q.empty())
	{
		int v = q.front();
		q.pop();
		d[v] = 0;
		forn(i, sz(g[v]))
		{
			int to = g[v][i];
			if (d[to] == -1)
			{
				q.push(to);
				d[to] = 0;
			}

		}

	}

}



int main() {
#ifdef _DEBUG
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
#endif
	int t;
	scanf("%d", &t);
	forn(ti, t)
	{
		int n, m;
		scanf("%d%d", &n, &m);
		vector<vector<int>> g(n, vi());

		cnt = 0;
		forn(i, m)
		{
			int v1, v2;
			scanf("%d%d", &v1, &v2);
			v1--, v2--;
			g[v1].push_back(v2);
			g[v2].push_back(v1);
		}
		vi d(n, -1);
		forn(i, n)
			if (d[i] == -1 && sz(g[i]) != 0)
			{
				bfs(i, g, d);
				break;
			}


		bool flag = true;
		forn(i, n)
		{
			if (sz(g[i]) > 0 && d[i] == -1)
				flag = false;
		}



		int x = 0, y = 0;
		forn(i, n)
		{
			int s = sz(g[i]);
			if (s != 0 && s % 2 == 0)
				x++;
			if (s % 2 == 1)
				y++;
		}


		if (y > 2 || !flag)
		{
			printf("NO\n");
			continue;
		}



		printf("YES\n");
	}

	return 0;
}